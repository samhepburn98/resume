import Paper from "@material-ui/core/Paper";
import styles from "./mobileExperienceTimeline.module.css";
import React from "react";

const MobileExperienceTimeline = () => {
    return (
        <div className={styles.mobileExperienceTimeline}>
            <h5 className={styles.timelineDate}>July 2021 - Present</h5>
            <Paper elevation={15} className={styles.paper}>
                <h4 className={styles.companyTitle}>KNOWLEDGE INTEGRATION</h4>
                <h5 className={styles.jobTitle}>Full-Stack Software Developer</h5>
                <p>Development of a full stack application using React and Grails.</p>
            </Paper>
            <h5 className={styles.timelineDate}>December 2020 - February 2021</h5>
            <Paper elevation={15} className={styles.paper}>
                <h4 className={styles.companyTitle}>GP LIAISON</h4>
                <h5 className={styles.jobTitle}>Freelance Data Analyst</h5>
                <p>
                    Worked with very large complex MySQL database tables containing sensitive data
                    referring to the UK health sector.
                </p>
                <p>
                    Created user facing analytics dashboards using AWS Quicksight in order to provide
                    useful insights to the clients.
                </p>
                <p>
                    Wrote Python scripts for general large scale database CRUD actions, and to
                    manipulate and cleanse inconsistent and potentially incorrect data from CSV files
                    provided by clients to ensure consistency and integrity of data in the database.
                </p>
            </Paper>
            <h5 className={styles.timelineDate}>July 2019 - July 2020</h5>
            <Paper elevation={15} className={styles.paper}>
                <h4 className={styles.companyTitle}>YORKSHIRE ENERGY</h4>
                <h5 className={styles.jobTitle}>Software Developer</h5>
                <p>Development of a full MEAN stack application used on a large screen in the office to
                    provide an interface for relevant staff news and event updates.
                </p>
                <p>
                    User interface development using Angular 8.
                </p>
                <p>Development of a microservice using Java/Spring Boot, AWS and SendGrid to
                    facilitate sending large volumes of emails from templates.
                </p>
                <p>General Python scripts written for various use cases including web scraping and
                    accessing API’s using REST.
                </p>
            </Paper>
        </div>
    )
}

export default MobileExperienceTimeline;