import styles from "./skill-icon-btn.module.css";
import PropTypes from "prop-types";
import Grid from "@material-ui/core/Grid";

const SkillIconBtn = ({src, alt, url}) => {
    return (
        <Grid
            className={styles.skillIconContainer}
            item
            xl={2} lg={2} md={2} sm={2} xs={2}
        >
            <img
                className={styles.skillIcon}
                src={src}
                alt={alt}
                onClick={() => window.open(url)}
            />
        </Grid>
    )
}

SkillIconBtn.propTypes =
    {
        src: PropTypes.string,
        alt: PropTypes.string,
        url: PropTypes.string
    }

export default SkillIconBtn;