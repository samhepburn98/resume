import {useEffect} from "react";
import PropTypes from 'prop-types';
import './circularText.css'


const CircularText = ({text, classIndex, radius}) => {
    let txt = text.split("")
    useEffect(() => {
        let clssIndex = document.getElementsByClassName("circTxt")[classIndex]
        let deg = 360 / text.length,
            origin = 0
        txt.forEach((ea) => {
            ea = `<p style='height:${radius}px;position:absolute;transform:rotate(${origin}deg);transform-origin:0 100%'>${ea}</p>`
            clssIndex.innerHTML += ea
            origin += deg
        })
    })

    return (
        <div className="container">
            <div className="circTxt" id="test"></div>
        </div>
    )
}

CircularText.propTypes = {
    text: PropTypes.string,
    radius: PropTypes.number,
    classIndex: PropTypes.number
}

export default CircularText;