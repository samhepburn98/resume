import styles from './experienceTimeline.module.css'
import {
    Timeline,
    TimelineItem,
    TimelineSeparator,
    TimelineConnector,
    TimelineContent,
    TimelineOppositeContent,
    TimelineDot
} from '@material-ui/lab';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import {BsFillLightningFill} from "react-icons/bs";
import {FaBookReader, FaSyringe} from "react-icons/fa";
import React from "react";

const ExperienceTimeline = () => {
    return (
        <div>
            <Timeline align="alternate">
                <TimelineItem>
                    <TimelineOppositeContent>
                        <Typography className={styles.timelineDate} variant="subtitle2">
                            July 2021 - Present
                        </Typography>
                    </TimelineOppositeContent>
                    <TimelineSeparator>
                        <TimelineDot className={styles.timelineDot}>
                            <FaBookReader/>
                        </TimelineDot>
                        <TimelineConnector/>
                    </TimelineSeparator>
                    <TimelineContent>
                        <Paper elevation={15} className={`${styles.paper} ${styles.paperLeftAlign}`}>
                            <h4 className={styles.companyTitle}>KNOWLEDGE INTEGRATION</h4>
                            <h5 className={styles.jobTitle}>Full-Stack Software Developer</h5>
                            <p>Development of a full stack application using React and Grails.</p>
                        </Paper>
                    </TimelineContent>
                </TimelineItem>
                <TimelineItem>
                    <TimelineOppositeContent>
                        <Typography className={styles.timelineDate} variant="subtitle2">
                            December 2020 - February 2021
                        </Typography>
                    </TimelineOppositeContent>
                    <TimelineSeparator>
                        <TimelineDot className={styles.timelineDot}>
                            <FaSyringe/>
                        </TimelineDot>
                        <TimelineConnector/>
                    </TimelineSeparator>
                    <TimelineContent>
                        <Paper elevation={15} className={styles.paper}>
                            <h4 className={styles.companyTitle}>GP LIAISON</h4>
                            <h5 className={styles.jobTitle}>Freelance Data Analyst</h5>
                            <p>
                                Worked with very large complex MySQL database tables containing sensitive data
                                referring to the UK health sector.
                            </p>
                            <p>
                                Created user facing analytics dashboards using AWS Quicksight in order to provide
                                useful insights to the clients.
                            </p>
                            <p>
                                Wrote Python scripts for general large scale database CRUD actions, and to
                                manipulate and cleanse inconsistent and potentially incorrect data from CSV files
                                provided by clients to ensure consistency and integrity of data in the database.
                            </p>
                        </Paper>
                    </TimelineContent>
                </TimelineItem>
                <TimelineItem>
                    <TimelineOppositeContent>
                        <Typography className={styles.timelineDate} variant="subtitle2">
                            July 2019 - July 2020
                        </Typography>
                    </TimelineOppositeContent>
                    <TimelineSeparator>
                        <TimelineDot className={styles.timelineDot}>
                            <BsFillLightningFill/>
                        </TimelineDot>
                    </TimelineSeparator>
                    <TimelineContent>
                        <Paper elevation={15} className={`${styles.paper} ${styles.paperLeftAlign}`}>
                            <h4 className={styles.companyTitle}>YORKSHIRE ENERGY</h4>
                            <h5 className={styles.jobTitle}>Software Developer</h5>
                            <p>Development of a full MEAN stack application used on a large screen in the office to
                                provide an interface for relevant staff news and event updates.
                            </p>
                            <p>
                                User interface development using Angular 8.
                            </p>
                            <p>Development of a microservice using Java/Spring Boot, AWS and SendGrid to
                                facilitate sending large volumes of emails from templates.
                            </p>
                            <p>General Python scripts written for various use cases including web scraping and
                                accessing API’s using REST.
                            </p>
                        </Paper>
                    </TimelineContent>
                </TimelineItem>
            </Timeline>
        </div>
    )
}

ExperienceTimeline.propTypes =
    {}

export default ExperienceTimeline;