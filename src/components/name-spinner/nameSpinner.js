import name1 from '../../resources/name-level-1-navy.svg'
import {ScrollRotate} from "react-scroll-rotate";
import './nameSpinner.css'
import PropTypes from "prop-types";

const NameSpinner = ({showSpinner}) => {
    if (showSpinner) {
        return (
            <div className={"spinnerContainer"}>
                <div className={"nameContainer"}>
                    <ScrollRotate animationDuration={0.15} method={"perc"} loops={2.5}>
                        <img className={"name"} src={name1} alt={'name'}/>
                    </ScrollRotate>
                </div>
            </div>
        )
    } else {
        return null
    }
}

NameSpinner.propTypes = {
    showSpinner: PropTypes.bool
}

export default NameSpinner;