import styles from './portfolio.module.css'
import {Fade} from "react-awesome-reveal";
import Grid from "@material-ui/core/Grid";
import React from "react";
import {Button, Paper} from "@material-ui/core";
import covidApp from "../../resources/covid-app-ss_galaxys4_black_portrait.png";
import cvApp from "../../resources/CV-app_macbookpro15_front.png";
import {FaGitlab} from 'react-icons/fa';

const Portfolio = () => {

    return (
        <div className={`fullPageView ${styles.portfolioView}`}>
            <Fade delay={500} cascade top triggerOnce>
                <h1 className={styles.pageTitle}>PORTFOLIO</h1>
                <Grid
                    alignItems="center"
                    className={styles.gridContainer}
                    container
                    justifyContent="center"
                >
                    <Grid item
                          xl={4} lg={4} md={5} sm={6} xs={10}
                    >
                        <Paper className={styles.itemCard}>
                            <img className={styles.itemImage} src={covidApp} alt={'Covid Application'}/>
                            <h6 className={styles.itemHeader}>Dissertation Covid-19 Android App</h6>
                            <h6 className={styles.itemSubheader}>KOTLIN / MONGODB </h6>
                            <p className={styles.itemText}>A mobile application detecting proximity between devices to
                                assist social distancing in the Covid-19 pandemic, written for my dissertation
                                project.</p>
                            <div className={styles.linkBtnContainer}>
                                <Button
                                    className={styles.linkBtn}
                                    color="default"
                                    onClick={() => window.open("https://gitlab.com/samhepburn98/resume")}
                                    startIcon={<FaGitlab/>}
                                    variant="contained"
                                >
                                    Gitlab
                                </Button>
                            </div>
                        </Paper>
                    </Grid>
                    <Grid item
                          xl={4} lg={4} md={5} sm={6} xs={10}
                    >
                        <Paper className={styles.itemCard}>
                            <img className={styles.itemImage} src={cvApp} alt={'CV Application'}/>
                            <h6 className={styles.itemHeader}>Resume Web App</h6>
                            <h6 className={styles.itemSubheader}>REACTJS</h6>
                            <p className={styles.itemText}>This very web application you're looking at right now!
                                A static web application to present my (fantastic) resume built using React.</p>
                            <div className={styles.linkBtnContainer}>
                                <Button
                                    className={styles.linkBtn}
                                    color="default"
                                    onClick={() => window.open("https://gitlab.com/samhepburn98/dissertation-project-covid-app")}
                                    startIcon={<FaGitlab/>}
                                    variant="contained"
                                >
                                    Gitlab
                                </Button>
                            </div>
                        </Paper>
                    </Grid>
                </Grid>
            </Fade>
            <p className={styles.comingSoon}>More coming soon...</p>
        </div>
    )
}

export default Portfolio;