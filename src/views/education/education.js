import styles from './education.module.css'
import Paper from '@material-ui/core/Paper';
import {useEffect, useState} from "react";
import useViewPort from "../../hooks/useViewPort";
import {Roll} from 'react-awesome-reveal';

const Education = () => {
    const [largerDimension, setLargerDimension] = useState("width")
    const {width, height} = useViewPort();

    useEffect(() => {
            if (typeof window !== "undefined") {
                if (width > height) {
                    setLargerDimension("width")
                } else {
                    setLargerDimension("height")
                }
            }
        }, [width, height]
    )
    return (
        <div className={`threeQuarterPageView ${styles.educationView}`}>
            <Roll left delay={500} triggerOnce>
                <Paper
                    className={`${styles.textContainer} ${largerDimension === "width" ? styles.greaterWidth : styles.greaterHeight}`}>
                    <div>
                        <h1 className={styles.educationTitle}>EDUCATION</h1>
                        <h4 className={styles.educationSubtitle}>University of Leeds<br/>2017 - 2021</h4>
                        <p className={styles.educationText}>Graduated Undergraduate Computer Science with Industry Year
                            (BSc) degree at the University of Leeds with a 2:1.</p>
                    </div>
                </Paper>
            </Roll>
        </div>
    )
}

export default Education;