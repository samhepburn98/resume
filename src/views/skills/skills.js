import styles from './skills.module.css'
import Grid from "@material-ui/core/Grid";
import React from "react";
import SkillIconBtn from "../../components/skill-icon-button";
import useViewPort from "../../hooks/useViewPort";
import {Zoom} from 'react-awesome-reveal';

const Skills = () => {
    const {width, height} = useViewPort()
    return (
        <div className={`fullPageView ${styles.skillsView}`}>
            <Grid
                alignItems="center"
                container
                className={width > height ? styles.gridContainer : styles.gridContainerRightAlign}
                justifyContent="center"
            >
                <Grid item
                      xl={4} lg={4} md={4} sm={2} xs={12}
                />
                <Grid item
                      xl={8} lg={8} md={8} sm={10} xs={12}
                      className={styles.skillsContainer}
                >
                    <Zoom cascade delay={500} duration={1500} triggerOnce>
                        <h1 className={styles.skillsTitle}>SKILLS</h1>
                        <h5>Languages</h5>
                        <Grid
                            container
                            direction="row"
                            justifyContent={width > height ? "flex-start" : "flex-end"}
                            alignItems="center"
                        >
                            <SkillIconBtn
                                src="https://upload.wikimedia.org/wikipedia/commons/6/61/HTML5_logo_and_wordmark.svg"
                                alt="html"
                                url="https://developer.mozilla.org/en-US/docs/Web/HTML"
                            />
                            <SkillIconBtn
                                src="https://upload.wikimedia.org/wikipedia/commons/d/d5/CSS3_logo_and_wordmark.svg"
                                alt="css"
                                url="https://developer.mozilla.org/en-US/docs/Web/CSS"
                            />
                            <SkillIconBtn
                                src="https://upload.wikimedia.org/wikipedia/en/thumb/3/30/Java_programming_language_logo.svg/80px-Java_programming_language_logo.svg.png"
                                alt="java"
                                url="https://www.java.com/en/"
                            />
                            <SkillIconBtn
                                src="https://upload.wikimedia.org/wikipedia/commons/3/3b/Javascript_Logo.png"
                                alt="js"
                                url="https://www.javascript.com/"
                            />
                            <SkillIconBtn
                                src="https://upload.wikimedia.org/wikipedia/commons/4/4c/Typescript_logo_2020.svg"
                                alt="ts"
                                url="https://www.typescriptlang.org/"
                            />
                            <SkillIconBtn
                                src="https://upload.wikimedia.org/wikipedia/commons/c/c3/Python-logo-notext.svg"
                                alt="python"
                                url="https://www.python.org/"
                            />
                        </Grid>
                        <h5>Frameworks & Tools</h5>
                        <Grid
                            container
                            direction="row"
                            justifyContent={width > height ? "flex-start" : "flex-end"}
                            alignItems="center"
                        >
                            <SkillIconBtn
                                src="https://www.vectorlogo.zone/logos/gitlab/gitlab-icon.svg"
                                alt="gitlab"
                                url="https://about.gitlab.com/"
                            />
                            <SkillIconBtn
                                src="https://www.vectorlogo.zone/logos/angular/angular-icon.svg"
                                alt="angular"
                                url="https://angular.io/"
                            />
                            <SkillIconBtn
                                src="https://www.vectorlogo.zone/logos/reactjs/reactjs-icon.svg"
                                alt="react"
                                url="https://reactjs.org/"
                            />
                            <SkillIconBtn
                                src="https://www.vectorlogo.zone/logos/nodejs/nodejs-icon.svg"
                                alt="node"
                                url="https://nodejs.org/en/"
                            />
                        </Grid>
                    </Zoom>
                </Grid>
            </Grid>
        </div>
    )
}

export default Skills;