import headshot from '../../resources/headshot_square.jpg'

import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import EmailIcon from '@material-ui/icons/Email';
import {FaGitlab} from 'react-icons/fa';

import styles from './header.module.css'
import {Zoom} from 'react-awesome-reveal';

const Header = () => {
    return (
        <Grid container className={styles.gridContainer}
              justifyContent={"center"}
              alignContent={"center"}
        >
            <Grid item
                  xl={6} lg={6} md={6} sm={12} xs={12}
            />
            <Grid item
                  xl={6} lg={6} md={6} sm={12} xs={12}
            >
                <Zoom delay={500} duration={1000} triggerOnce>
                    <img src={headshot} className={styles.headshot} alt={"The beautiful Sam Hepburn"}/>
                </Zoom>
            </Grid>
            <Grid item
                  xl={6} lg={6} md={6} sm={12} xs={12}
            />
            <Grid item
                  xl={6} lg={6} md={6} sm={12} xs={12}
            >
                <Zoom delay={500} duration={1250} triggerOnce>
                    <h2 className={styles.fullName}>SAM HEPBURN</h2>
                </Zoom>
                <Zoom delay={500} duration={1500} triggerOnce>
                    <p className={styles.bio}>I'm a Full-Stack Software Developer, currently based in Sheffield
                        working
                        for Knowledge Integration Ltd.</p>
                </Zoom>
            </Grid>
            <Grid item
                  xl={6} lg={6} md={6} sm={12} xs={12}
            />
            <Grid item
                  xl={6} lg={6} md={6} sm={12} xs={12}
            >
                <Zoom delay={500} duration={1500} triggerOnce>
                    <span>
                        <IconButton onClick={() => window.open("https://www.gitlab.com/samhepburn98")}
                                    className={styles.iconBtn}>
                                <FaGitlab className={styles.gitlabIcon}/>
                        </IconButton>
                        <IconButton onClick={() => window.open("https://uk.linkedin.com/in/sam-hepburn")}
                                    className={styles.iconBtn}>
                            <LinkedInIcon href="https://www.gitlab.com/samhepburn98" fontSize={"large"}/>
                        </IconButton>
                        <IconButton onClick={() => window.location.href = "mailto: samhepburn98@msn.com"}
                                    className={styles.iconBtn}>
                            <EmailIcon fontSize={"large"}/>
                        </IconButton>
                    </span>
                </Zoom>
            </Grid>
        </Grid>
    )
}

export default Header;