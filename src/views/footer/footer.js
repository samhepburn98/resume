import styles from './footer.module.css'
import Grid from "@material-ui/core/Grid";

const Footer = () => {
    return (
        <div className={styles.footerContainer}>
            <Grid container>
                <Grid item
                      xl={12} lg={12} md={12} sm={12} xs={12}
                >
                    I'm available for freelance work.<br/>
                    If you have a project in mind, believe you require my assistance, or simply want to
                    say hello and exchange a meme or two, please feel free to get in touch.
                </Grid>
            </Grid>
        </div>
    )
}

export default Footer;