import styles from './experience.module.css'
import ExperienceTimeline from "../../components/experience-timeline";
import useViewPort from "../../hooks/useViewPort";
import MobileExperienceTimeline from "../../components/mobile-experience-timeline";
import {Fade} from 'react-awesome-reveal';

const Experience = () => {
    const {width, height} = useViewPort()
    return (
        <div className={`fullPageView ${styles.experienceView}`}>
            <Fade delay={500} top triggerOnce>
                <h1 className={styles.pageTitle}>EXPERIENCE</h1>
            </Fade>
            <Fade delay={500} bottom triggerOnce>
                {width > height ? <ExperienceTimeline/> : <MobileExperienceTimeline/>}
            </Fade>
        </div>
    )
}

export default Experience;