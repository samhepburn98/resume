import './App.css';
import NameSpinner from "./components/name-spinner";
import Header from "./views/header/header";
import IconButton from "@material-ui/core/IconButton";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";

import {Link} from 'react-scroll'
import Footer from "./views/footer";
import Experience from "./views/experience";
import Skills from "./views/skills";
import Education from "./views/education";
import Portfolio from "./views/portfolio";
import {useEffect, useState} from "react";
import useViewPort from "./hooks/useViewPort";

function App() {
    const [showSpinner, setShowSpinner] = useState(true)
    const {width, height} = useViewPort()
    useEffect(() => {
            if (typeof window !== "undefined") {
                window.onscroll = () => {
                    let currentScrollPos = window.pageYOffset;
                    if (currentScrollPos > height * 2) {
                        setShowSpinner(false)
                    } else {
                        setShowSpinner(true)
                    }
                }
            }
        }
    )
    return (
        <div className="app">
            {width > height ? <NameSpinner showSpinner={showSpinner}/> : <></>}
            <header className="fullPageView appHeader">
                <Header/>
            </header>
            <div className={"expand"}>
                <Link to="skills" spy={true} smooth={true}>
                    <IconButton className={"expandBtn"}>
                        <ExpandMoreIcon/>
                    </IconButton>
                </Link>
            </div>
            <div id={"skills"}>
                <Skills/>
            </div>
            <Experience className="test"/>
            <Education/>
            <Portfolio/>
            <footer className={"appFooter"}>
                <Footer/>
            </footer>
        </div>
    );
}

export default App;
